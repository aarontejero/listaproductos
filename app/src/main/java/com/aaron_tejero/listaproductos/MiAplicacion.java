package com.aaron_tejero.listaproductos;

import android.app.Application;

import com.facebook.stetho.Stetho;

/**
 * Created by AaronTejero on 16/11/2016.
 */
public class MiAplicacion extends Application {
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }
}
