package com.aaron_tejero.listaproductos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class DescripcionProductos extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private long productoId;
    private TextView nombre;
    private TextView descripcion;
    private TextView modelo;
    private TextView serie;
    private TextView marca;

    public static final String EXTRA_PRODUCTO_ID="producto.id.extra";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descripcion_productos);


        Intent intent = getIntent();
        productoId=intent.getLongExtra(MainActivity.EXTRA_ID_PRODUCTO,-1L);
        nombre=(TextView) findViewById(R.id.nombreProducto);
        descripcion=(TextView) findViewById(R.id.descripcionProducto);
        modelo=(TextView) findViewById(R.id.modeloProducto);
        serie=(TextView) findViewById(R.id.serieProducto);
        marca=(TextView) findViewById(R.id.marcaProducto);

        getSupportLoaderManager().initLoader(0, null, this);

        findViewById(R.id.botoneliminar).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new DeleteProductoTask(DescripcionProductos.this, productoId).execute();
                    }
                }
        );

        findViewById(R.id.botonactualizar).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent= new Intent(DescripcionProductos.this,AgregarActivity.class);
                        intent.putExtra(EXTRA_PRODUCTO_ID,productoId);
                        startActivity(intent);
                    }
                });
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new ProductosDescLoader(this,productoId);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if(data!=null && data.moveToFirst()){
            String nombreProducto = data.getString(data.getColumnIndexOrThrow(ProductosDB.COL_NAME));
            String descProducto = data.getString(data.getColumnIndexOrThrow(ProductosDB.COL_DESCRIPCION));
            String modeloProducto = data.getString(data.getColumnIndexOrThrow(ProductosDB.COL_MODELO));
            String serieProducto = data.getString(data.getColumnIndexOrThrow(ProductosDB.COL_SERIE));
            String marcaProducto = data.getString(data.getColumnIndexOrThrow(ProductosDB.COL_MARCA));
            nombre.setText(nombreProducto);
            descripcion.setText(descProducto);
            modelo.setText(modeloProducto);
            serie.setText(serieProducto);
            marca.setText(marcaProducto);

        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    public static class DeleteProductoTask extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<Activity> weakActivity;
        long productoId;

        public DeleteProductoTask(Activity activity, long id) {
            weakActivity = new WeakReference<Activity>(activity);
            productoId = id;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Context context = weakActivity.get();

            if (context == null) {
                return false;
            }

            Context appContext = context.getApplicationContext();

            int filasafectadas = ProductosDB.eliminaConId(appContext, productoId);
            return (filasafectadas != 0);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Activity context = weakActivity.get();
            if (context == null) {
                return;
            }
            if (aBoolean) {
                Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();
            }
            context.finish();
        }

    }
}
