package com.aaron_tejero.listaproductos;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.support.v4.content.LocalBroadcastManager;

/**
 * Created by AaronTejero on 16/11/2016.
 */
public class ProductosDB extends SQLiteOpenHelper {

    private static final int DB_VERSION=1;
    private static final String DB_NAME="productos.db";
    private static final String TABLE_NAME="productos";

    public  static final String COL_NAME="nombre";
    public  static final String COL_DESCRIPCION="descripcion";
    public  static final String COL_MODELO="modelo";
    public  static final String COL_SERIE="serie";
    public  static final String COL_MARCA="marca";


    public ProductosDB(Context context ) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createQuery=
                "CREATE TABLE "+TABLE_NAME+
                        " (_id INTEGER PRIMARY KEY, "
                        + COL_NAME + " TEXT NOT NULL COLLATE UNICODE, "
                        + COL_DESCRIPCION + " TEXT NOT NULL, "
                        + COL_MODELO + " TEXT NOT NULL, "
                        + COL_SERIE + " TEXT, "
                        + COL_MARCA + " TEXT)";
        db.execSQL(createQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String upgradeQuery="DROP TABLE IF EXIST"+ TABLE_NAME;
        db.execSQL(upgradeQuery);
    }


    public static long insertaAnimal (Context context, String nombre, String descripcion, String modelo, String serie, String marca){

        SQLiteOpenHelper dbOpenHelper= new ProductosDB(context);
        SQLiteDatabase database= dbOpenHelper.getWritableDatabase();

        ContentValues valorProducto=new ContentValues();
        valorProducto.put(COL_NAME,nombre);
        valorProducto.put(COL_DESCRIPCION,descripcion);
        valorProducto.put(COL_MODELO,modelo);
        valorProducto.put(COL_SERIE,serie);
        valorProducto.put(COL_MARCA,marca);
        long result=-1L;
        try {
            result= database.insert(TABLE_NAME,null,valorProducto);
            if (result!=-1L){

                LocalBroadcastManager broadcastManager=LocalBroadcastManager.getInstance(context);
                Intent intentFilter= new Intent(ProductosLoader.ACTION_RELOAD_TABLE);
                broadcastManager.sendBroadcast(intentFilter);
            }
        } finally {
            dbOpenHelper.close();
        }


        return result;

    }

    public static Cursor devuelveTodos(Context context){
        SQLiteOpenHelper dbOpenHelper= new ProductosDB(context);
        SQLiteDatabase database= dbOpenHelper.getReadableDatabase();


        return database.query(TABLE_NAME, new String[] {COL_NAME, COL_DESCRIPCION, COL_MODELO, COL_SERIE, COL_MARCA, BaseColumns._ID}, null,null,null, null, COL_NAME+" ASC" );
    }

    public static Cursor devuelveConId(Context context, long identificador){
        SQLiteOpenHelper dbOpenHelper= new ProductosDB(context);
        SQLiteDatabase database= dbOpenHelper.getReadableDatabase();

        return database.query(
                TABLE_NAME,
                new String[] {COL_NAME, COL_DESCRIPCION,COL_MODELO,COL_SERIE,COL_MARCA, BaseColumns._ID},
                BaseColumns._ID+" = ?",
                new String[] {String.valueOf(identificador)},
                null, null,
                COL_NAME+" ASC" );
    }

    public static int eliminaConId(Context context, long productoId){
        SQLiteOpenHelper dbOpenHelper= new ProductosDB(context);
        SQLiteDatabase database= dbOpenHelper.getWritableDatabase();

        int resultado=database.delete(
                TABLE_NAME,
                BaseColumns._ID+ " = ?",
                new String[]{String.valueOf(productoId)});

        if (resultado!=0){

            LocalBroadcastManager broadcastManager=LocalBroadcastManager.getInstance(context);
            Intent intentFilter= new Intent(ProductosLoader.ACTION_RELOAD_TABLE);
            broadcastManager.sendBroadcast(intentFilter);
        }
        dbOpenHelper.close();
        return resultado;
    }

    public static int actualizaProducto (Context context, String nombre, String descripcion,String modelo, String serie, String marca, long id){

        SQLiteOpenHelper dbOpenHelper= new ProductosDB(context);
        SQLiteDatabase database= dbOpenHelper.getWritableDatabase();

        ContentValues valorProducto=new ContentValues();
        valorProducto.put(COL_NAME,nombre);
        valorProducto.put(COL_DESCRIPCION,descripcion);
        valorProducto.put(COL_MODELO,modelo);
        valorProducto.put(COL_SERIE,serie);
        valorProducto.put(COL_MARCA,marca);


        int  result= database.update(TABLE_NAME,
                valorProducto,
                BaseColumns._ID + " = ?",
                new String[]{String.valueOf(id)});

        if (result!=0){
            LocalBroadcastManager broadcastManager=LocalBroadcastManager.getInstance(context);
            Intent intentFilter= new Intent(ProductosLoader.ACTION_RELOAD_TABLE);
            broadcastManager.sendBroadcast(intentFilter);
        }

        dbOpenHelper.close();
        return result;

    }


}
