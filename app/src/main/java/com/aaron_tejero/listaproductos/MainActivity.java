package com.aaron_tejero.listaproductos;

import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {
    public final static String EXTRA_ID_PRODUCTO="Productos.ID_PRODUCTO";
    private SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView lista= (ListView) findViewById(R.id.activity_main);
        adapter =new SimpleCursorAdapter(
                this,
                android.R.layout.simple_list_item_1,
                null,
                new String[]{ProductosDB.COL_NAME},
                new int[]{android.R.id.text1},
                0);
        lista.setAdapter(adapter);

        getSupportLoaderManager().initLoader(0, null, this);
        lista.setOnItemClickListener(this);


        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, AgregarActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(this, DescripcionProductos.class);

        intent.putExtra(EXTRA_ID_PRODUCTO, adapter.getItemId(position));
        startActivity(intent);
    }
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new ProductosLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }


}
