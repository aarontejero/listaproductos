package com.aaron_tejero.listaproductos;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class AgregarActivity extends AppCompatActivity implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor> {
    private long productoId;
    private EditText nombre;
    private EditText desc;
    private EditText modelo;
    private EditText serie;
    private EditText marca;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar);

        nombre= (EditText) findViewById(R.id.et_Nombre);
        desc= (EditText) findViewById(R.id.et_Descripcion);
        modelo= (EditText) findViewById(R.id.et_Modelo);
        serie= (EditText) findViewById(R.id.et_Serie);
        marca= (EditText) findViewById(R.id.et_Marca);
        productoId=getIntent().getLongExtra(DescripcionProductos.EXTRA_PRODUCTO_ID,-1L);
        if (productoId!=-1L){
            getSupportLoaderManager().initLoader(0,null,this);
        }
        findViewById(R.id.btn_Agregar).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {


        String nombreProducto=nombre.getText().toString();
        String descProducto=desc.getText().toString();
        String modeloProducto=modelo.getText().toString();
        String serieProducto=serie.getText().toString();
        String marcaProducto=marca.getText().toString();

        if (nombreProducto.isEmpty() || descProducto.isEmpty() || modeloProducto.isEmpty()){
            Toast.makeText(this, "Los campos Nombre, Descricpion y Modelo no pueden ser vacios", Toast.LENGTH_SHORT).show();
        }
        else {

            new CreateAnimalTask(this, nombreProducto,descProducto,modeloProducto,serieProducto,marcaProducto, productoId).execute();
        }



    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new ProductosDescLoader(this,productoId);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data != null && data.moveToFirst()){
            String nombreProducto = data.getString(data.getColumnIndexOrThrow(ProductosDB.COL_NAME));
            String descProducto = data.getString(data.getColumnIndexOrThrow(ProductosDB.COL_DESCRIPCION));
            String modeloProducto = data.getString(data.getColumnIndexOrThrow(ProductosDB.COL_MODELO));
            String serieProducto = data.getString(data.getColumnIndexOrThrow(ProductosDB.COL_SERIE));
            String marcaProducto = data.getString(data.getColumnIndexOrThrow(ProductosDB.COL_MARCA));
            nombre.setText(nombreProducto);
            desc.setText(descProducto);
            modelo.setText(modeloProducto);
            serie.setText(serieProducto);
            marca.setText(marcaProducto);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }


    public static class CreateAnimalTask extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<Activity> weakActivity;
        private String productoName;
        private String productoDesc;
        private String productoModelo;
        private String productoSerie;
        private String productoMarca;
        private long productoId;

        public CreateAnimalTask(Activity activity, String name, String desc,  String modelo, String serie, String marca, long productoId){
            weakActivity = new WeakReference<Activity>(activity);
            productoName = name;
            productoDesc = desc;
            productoModelo= modelo;
            productoSerie=serie;
            productoMarca=marca;
            this.productoId=productoId;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Context context = weakActivity.get();
            if (context == null){
                return false;
            }

            Context appContext = context.getApplicationContext();

            Boolean success=false;
           if (productoId!=-1L){
               int filasAfectadas=ProductosDB.actualizaProducto(appContext, productoName,productoDesc,productoModelo,productoSerie,productoMarca,productoId);
               success=(filasAfectadas!=0);
            }else {
                long id = ProductosDB.insertaAnimal(appContext, productoName, productoDesc,productoModelo,productoSerie,productoMarca);
                success=(id!=-1L);
           }
            return success;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Activity context = weakActivity.get();
            if(context == null){
                return;
            }
            if (aBoolean){
                Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();
            }
            context.finish();
        }
    }


}
